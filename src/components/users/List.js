import React, {useContext} from "react";
import UsersContext from "../../context/user-context"

const List = () => {
  return (
    <UsersContext.Consumer>
      {(value, index) => <div>
        {value.map(user => 
          <div key={`${index}-${user.name.first}`}>{user.name.first}</div>  
        )}
      </div>}
    </UsersContext.Consumer>
  )
}

export default List;