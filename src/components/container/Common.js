import React, { useState, useEffect } from "react";
import Users from "../users/List";
import Counter from "../counter/Counter";
import UsersContext from "../../context/user-context"
import { fetchUsers } from "../../api/users";

let usersDummy = []

const getUsersList = (callback) => {
  let result = [];
  fetchUsers().then(res => {
    console.log("res.results: ", res.results)
    callback(res.results)
    result = res
  });
  return result
};

const Common = () => {
  const [users, setUsers] = useState(usersDummy);

  useEffect(() => {
    getUsersList(setUsers)
  }, []) // empty array - componentDidMounte()

  return (
    <div className="context-wrapper"> 
      <UsersContext.Provider value={users}>
        <Users />
        <div style={{ marginBottom: '5px', marginTop: "5px" }}>	&nbsp;</div>
        <Counter />
      </UsersContext.Provider>
    </div>
  )
}

export default Common;