import React, { useContext } from "react"
import UsersContext from "../../context/user-context";

const Counter = () => {

  const users = useContext(UsersContext)

  const usersList = users.map((user, index) => (
    <div className="avatar" key={`${index}-${user.first_name}`}>
      <div className="mb-8 rounded-btn w-24 h-24">
        <img src={`${user.picture.thumbnail}`} />
        <div>
          <span className="text-xs">{user.name.first} </span>
          <span className="text-xs">{user.name.last}</span>
        </div>
      </div>
    </div>
  ))

  return (
    <>
      <div className="my-superclass">
        {usersList}
      </div>
      <div>
      <div style={{ marginBottom: '5px', marginTop: "5px" }}>	&nbsp;</div>
        {users.map((user, index) => (
          <div key={`${index}-${user.first_name}`}>
            <span>{user.name.first} </span>
            <span>{user.name.last}</span>
          </div>
        ))}
      </div>
    </>
  )
}


export default Counter;