import logo from './logo.svg';
import './App.css';
import Common from "./components/container/Common";

function App() {
  const value = 'My Context Value';
  return (
    <div className="App">
      <header className="App-header">learn react
        <Common /> 
      </header>
    </div>
  );
}
// npm run test -- --watchAll=false

export default App;
