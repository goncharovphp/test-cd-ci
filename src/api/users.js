const url = "https://randomuser.me/api/?results=10";

export const fetchUsers = async () => {
  const res = await fetch(url);
  return res.json()
}